let head =
`/* O.i18n
 * Copyright © 2018 ⊕ OtagJS (otagjs.org), Licensed under MIT
 * (https://belge.otagjs.org/en/lib/i18n)
 */
`
  , fs = require('fs')
  , file = fs.readFileSync('index.min.js', 'utf-8')

file = head + file.substr(0, file.length - 39)

fs.writeFileSync('index.min.js', file)