# O.u18a ya da O.i18n

[English Documentation](https://belge.otagjs.org/en/lib/i18n/)

---

O.u18a (uluslararasılaştırma) diller arası çeviri için yalınlaştırılmış bir arabirim sunar

**ES6 Bileşeni** olarak kullanabileceğiniz **O.u18a**'yı ön uçta kullanabilirsiniz.

**Node Bileşeni** olarak arka uçta da kullanabilirsiniz.

## Kurulum 

```bash
  npm i o.i18n -g
```

### ES6 Bileşeni olarak kullanma

Örnek kullanım ./orn/frontend dizini içindedir.

##### Dil belgesi oluşturma

```javascript
  // tr.js
  export default {
    sayı: ['sıfır', 'bir', 'iki'],
    birim: {
      m : 'metre',
      km: 'kilometre'
    }
  }
```

##### Kullanma

```javascript   
  import u18a from 'o.i18n'
  import tr   from './tr.js'
  //import en from './en.js'
  
  i18n({
    tr /*,en*/
  })

  console.log( 'sayı.0'.tr )           // sıfır
  console.log( 'birim.km'.u18a('tr') ) // kilometre
```

### NodeJS Bileşeni olarak kullanma

Örnek kullanım ./orn/backend dizini içindedir.

##### Dil belgesi oluşturma

```javascript
  // tr.js
  module.exports = {
  sayı: ['sıfır', 'bir', 'iki'],
  birim: {
    m : 'metre',
    km: 'kilometre'
  }
}
```

##### Kullanım

```javascript
  const u18a = require('o.i18n')
  const tr   = require('./tr')
  //const en = require('./en')
  

  u18a({
    tr /*, en*/
  })
```

#### Geleneksel kullanım biçimi

HTML belgenize ekleyin

```html
<script src="https://cdn.jsdelivr.net/npm/o.i18n@1.0.0/index.min.js"></script>
```

## Sınama

Geliştirme ortamında sınayın

```bash
  # Geliştirme bağımlılıklarını kur
  npm i

  # Sına
  npm test
```


## Yeterge

MIT Özgür Yazılım Yetergesi

![](https://nodei.co/npm/o.i18n.png)