/*  O.i18n
 *  Copyright © 2018 ⊕ OtagJS (otagjs.org), Licensed under MIT
 *  (https://belge.otagjs.org/en/lib/i18n)
 * * *
 *  O.u18a
 *  İyelik Bildirisi © 2018 ⊕ Otağ (otagjs.org), MIT Yetergesi ile dağıtılmaktadır
 *  (https://belge.otagjs.org/tr/lib/u18a)
**/

let i18n, u18a

i18n = u18a = (Ayar)=>{
  let root = typeof window == 'undefined' ? global : window
  Object.defineProperties(
    root.String.prototype,
    Object.keys(Ayar).reduce((u, dil)=>{
      u[dil] = {
        get() {
          let deyiş = Ayar[dil]
          return this.split('.').reduce((NULL, açar)=>{
            return deyiş[açar] ? (deyiş = deyiş[açar]) : this
          }, '')
        }
      }
      return u
    }, {})
  )
  root.String.prototype.u18a = root.String.prototype.i18n = function(dil) {
    return this[dil]
  }
}
if(typeof module != 'undefined'){module.exports=u18a}
export default u18a